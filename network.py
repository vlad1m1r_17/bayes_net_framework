from networkx import *
import pandas as pd
import matplotlib.pyplot as plt
from node import Node, PROB
from copy import deepcopy


class Network:
    def __init__(self):
        self._net = nx.DiGraph()

    def _get_node_info(self, graph_node) -> (Node, list):
        node = self._net.nodes[graph_node]['data']
        pred = list(self._net.predecessors(graph_node))
        return node, pred

    def add_node(self, node: Node):
        self._net.add_node(node.name, data=deepcopy(node))

    def _check_graph(self):
        for graph_node in self._net.nodes:
            node, pred = self._get_node_info(graph_node)
            cpt = node.cpt
            assert graph_node == node.name
            desired_size = 1
            for p in pred:
                desired_size *= len(self._net.nodes[p]['data'].possible_values)
            assert cpt.shape[0] == desired_size * len(node.possible_values)
            by = [col for col in cpt.columns.tolist() if col not in [PROB, node.name]]
            if len(by) > 0:
                sum_probs = cpt.groupby(by=by) \
                    .sum()[PROB].sum()
            else:
                sum_probs = cpt[PROB].sum()
            assert float(sum_probs) == float(desired_size)

    def build_net(self):
        for graph_node in self._net.nodes:
            node, _ = self._get_node_info(graph_node)
            cpt = node.cpt
            pred = [col for col in cpt.columns.tolist() if col not in [PROB, node.name]]
            for p in pred:
                self._net.add_edge(p, graph_node)
        self._check_graph()

    def show(self):
        nx.draw_networkx(self._net, with_labels=True, font_weight='bold')
        ax = plt.gca()
        ax.margins(0.20)
        plt.axis("off")
        plt.show()
        plt.show()

    @classmethod
    def join_factors(cls, left: pd.DataFrame, right: pd.DataFrame) -> pd.DataFrame:
        on = [col for col in left.columns.tolist() if col in right.columns.tolist() and col != PROB]
        if len(on) == 0:
            return left
        res = left.set_index(on).join(right.set_index(on), on=on, lsuffix='_l', rsuffix='_r')
        res[PROB] = res[PROB + '_l'] * res[PROB + '_r']
        res.drop([PROB + '_l', PROB + '_r'], axis=1, inplace=True)
        res.reset_index(inplace=True)
        return res

    @classmethod
    def eliminate_variable(cls, factor: pd.DataFrame, variable: str) -> pd.DataFrame:
        cols = [col for col in factor.columns.tolist() if col not in [variable, PROB]]
        res = factor.groupby(cols).agg('sum')
        res.reset_index(inplace=True)
        return res

    def get_probs(self, variable: str, evidence: dict):

        def shrink_factor(factor, evidence_name, evidence_value):
            return factor[factor[evidence_name] == evidence_value]

        node, _ = self._get_node_info(variable)
        factors = {n: deepcopy(self._net.nodes[n]['data'].cpt) for n in self._net.nodes}
        for var in evidence:
            if var not in self._net.nodes:
                raise RuntimeError(f'No such node in net {var}')
            factors[var] = shrink_factor(factors[var], var, evidence[var])
            for s in self._net.successors(var):
                factors[s] = shrink_factor(factors[s], var, evidence[var])
        hidden_variables = [i for i in self._net.nodes if i not in evidence and i != variable]
        factors = list(factors.values())
        for hidden_var in hidden_variables:
            to_join_ind = []
            for i in range(len(factors)):
                if hidden_var in factors[i].columns.tolist():
                    to_join_ind.append(i)
            if len(to_join_ind) >= 2:
                res = self.join_factors(factors[to_join_ind[0]], factors[to_join_ind[1]])
                for ind in to_join_ind[2:]:
                    res = self.join_factors(res, factors[ind])
            elif len(to_join_ind) == 1:
                res = factors[to_join_ind[0]]
            else:
                raise RuntimeError()
            for ind in to_join_ind[::-1]:
                factors.pop(ind)
            res = self.eliminate_variable(res, hidden_var)
            factors.append(res)

        target = deepcopy(factors[0])
        factors.pop(0)
        for i in factors:
            target = Network.join_factors(target, i)
        sums = target[PROB].sum()
        target[PROB] = target[PROB]/sums
        return target
