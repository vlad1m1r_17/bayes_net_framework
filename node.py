import pandas as pd
PROB = 'prob'



class Node:
    def __init__(self, name: str, possible_values: list, cpt: list):
        if name == 'prob':
            raise AttributeError('Invalid value for attribute name')
        self._name = name
        self._possible_values = possible_values
        self._init_cpt(cpt)

    def show(self):
        print(self._name)
        print(self._cpt)

    def _init_cpt(self, new_cpt):
        if isinstance(new_cpt, list):
            if all(isinstance(i, list) for i in new_cpt):
                header = new_cpt[0] + ['prob']
                if self._name not in header:
                    raise AttributeError('Invalid cpt header. Hint: node name must be in the header')
                self._cpt = pd.DataFrame(new_cpt[1:], columns=header)
                if set(self._cpt[self._name].unique()) != set(self._possible_values):
                    raise RuntimeError(f'Invalid values for attribute {self._name} in cpt')

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, new_name):
        self._name = new_name

    @property
    def cpt(self):
        return self._cpt

    @cpt.setter
    def cpt(self, new_cpt):
        self._init_cpt(new_cpt)

    @property
    def possible_values(self):
        return self._possible_values
