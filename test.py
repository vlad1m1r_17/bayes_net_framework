from network import Network, Node

net = Network()
r = Node('r', ['+r', '-r'],
               cpt=
         [['r'],
          ['+r', 0.1],
          ['-r', 0.9]])

t = Node('t', ['+t', '-t'],
         cpt=
         [['r', 't'],
          ['+r', '+t', 0.8],
          ['+r', '-t', 0.2],
          ['-r', '+t', 0.1],
          ['-r', '-t', 0.9]])

l = Node('l', ['+l', '-l'],
         cpt=
         [['t', 'l'],
          ['+t', '+l', 0.3],
          ['+t', '-l', 0.7],
          ['-t', '+l', 0.1],
          ['-t', '-l', 0.9]])

net.add_node(r)
net.add_node(t)
net.add_node(l)
net.build_net()
net.show()

print(net.get_probs('l', {'r': '+r'}))
