## Simple framework for Bayes network inference using variable elimination algorithm

NOTE: This project was done only for educational purposes. DO NOT USE IT AS READY SOLUTION IN PRODUCTION ENVIRONMENTS OR AS GROUND TRUTH IMPLEMENTATION!!!


# Structure

- Node class that stores name, possible values an CPT
- Network class that constructs network from added Nodes and performs some obvious checks
- test.py with very simple network
- task.py with a little larger network and some queries