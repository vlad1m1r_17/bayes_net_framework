from network import Network, Node


if __name__ == '__main__':
    net = Network()

    age = Node(name='age', possible_values=['between18and30', 'between30and50', 'morethan50'],
               cpt=
               [['age'],
                ['between18and30',  0.2],
                ['between30and50',  0.5],
                ['morethan50',      0.3]])

    guarantor = Node(name='guarantor', possible_values=['yes', 'no'],
                     cpt=
                     [['guarantor'],
                      ['yes',       0.3],
                      ['no',        0.7]])

    higherEducation = Node(name='higherEducation', possible_values=['yes', 'no'],
                           cpt=
                           [['higherEducation'],
                            ['yes',             0.7],
                            ['no',              0.3]])

    income = Node(name='income', possible_values=['less1000', 'between1000and3000', 'between3000and8000', 'more8000'],
                  cpt=
                  [['income'],
                   ['less1000',             0.1],
                   ['between1000and3000',   0.2],
                   ['between3000and8000',   0.5],
                   ['more8000',             0.2]])

    married = Node(name='married', possible_values=['yes', 'no'],
                   cpt=
                   [['married'],
                    ['yes',     0.5],
                    ['no',      0.5]])

    ratioOfDebts = Node(name='ratioOfDebts', possible_values=['High', 'Low'],
                        cpt=
                        [['ratioOfDebts'],
                         ['High',           0.6],
                         ['Low',            0.4]])

    assets = Node(name='assets', possible_values=['High', 'Medium', 'Low'],
                  cpt=
                  [['income',               'assets'],
                   ['less1000',             'High',     0.1],
                   ['less1000',             'Medium',   0.3],
                   ['less1000',             'Low',      0.6],
                   ['between1000and3000',   'High',     0.2],
                   ['between1000and3000',   'Medium',   0.5],
                   ['between1000and3000',   'Low',      0.3],
                   ['between3000and8000',   'High',     0.7],
                   ['between3000and8000',   'Medium',   0.2],
                   ['between3000and8000',   'Low',      0.1],
                   ['more8000',             'High',     0.8],
                   ['more8000',             'Medium',   0.15],
                   ['more8000',              'Low',     0.05]])

    futureIncome = Node(name='futureIncome', possible_values=['Promising', 'NotPromising'],
                        cpt=
                        [['assets', 'income',               'higherEducation',  'futureIncome'],
                         ['High',   'less1000',             'yes',              'Promising',    0.1],
                         ['High',   'less1000',             'yes',              'NotPromising', 0.9],
                         ['High',   'less1000',             'no',               'Promising',    0.05],
                         ['High',   'less1000',             'no',               'NotPromising', 0.95],
                         ['High',   'between1000and3000',   'yes',              'Promising',    0.3],
                         ['High',   'between1000and3000',   'yes',              'NotPromising', 0.7],
                         ['High',   'between1000and3000',   'no',               'Promising',    0.2],
                         ['High',   'between1000and3000',   'no',               'NotPromising', 0.8],
                         ['High',   'between3000and8000',   'yes',              'Promising',    0.6],
                         ['High',   'between3000and8000',   'yes',              'NotPromising', 0.4],
                         ['High',   'between3000and8000',   'no',               'Promising',    0.51],
                         ['High',   'between3000and8000',   'no',               'NotPromising', 0.49],
                         ['High',   'more8000',             'yes',              'Promising',    0.9],
                         ['High',   'more8000',             'yes',              'NotPromising', 0.1],
                         ['High',   'more8000',             'no',               'Promising',    0.7],
                         ['High',   'more8000',             'no',               'NotPromising', 0.3],
                         ['Medium', 'less1000',             'yes',              'Promising',    0.05],
                         ['Medium', 'less1000',             'yes',              'NotPromising', 0.95],
                         ['Medium', 'less1000',             'no',               'Promising',    0.03],
                         ['Medium', 'less1000',             'no',               'NotPromising', 0.97],
                         ['Medium', 'between1000and3000',   'yes',              'Promising',    0.25],
                         ['Medium', 'between1000and3000',   'yes',              'NotPromising', 0.75],
                         ['Medium', 'between1000and3000',   'no',               'Promising',    0.15],
                         ['Medium', 'between1000and3000',   'no',               'NotPromising', 0.85],
                         ['Medium', 'between3000and8000',   'yes',              'Promising',    0.52],
                         ['Medium', 'between3000and8000',   'yes',              'NotPromising', 0.48],
                         ['Medium', 'between3000and8000',   'no',               'Promising',    0.44],
                         ['Medium', 'between3000and8000',   'no',               'NotPromising', 0.56],
                         ['Medium', 'more8000',             'yes',              'Promising',    0.8],
                         ['Medium', 'more8000',             'yes',              'NotPromising', 0.2],
                         ['Medium', 'more8000',             'no',               'Promising',    0.67],
                         ['Medium', 'more8000',             'no',               'NotPromising', 0.33],
                         ['Low',    'less1000',             'yes',              'Promising',    0.02],
                         ['Low',    'less1000',             'yes',              'NotPromising', 0.98],
                         ['Low',    'less1000',             'no',               'Promising',    0.01],
                         ['Low',    'less1000',             'no',               'NotPromising', 0.99],
                         ['Low',    'between1000and3000',   'yes',              'Promising',    0.1],
                         ['Low',    'between1000and3000',   'yes',              'NotPromising', 0.9],
                         ['Low',    'between1000and3000',   'no',               'Promising',    0.07],
                         ['Low',    'between1000and3000',   'no',               'NotPromising', 0.93],
                         ['Low',    'between3000and8000',   'yes',              'Promising',    0.48],
                         ['Low',    'between3000and8000',   'yes',              'NotPromising', 0.52],
                         ['Low',    'between3000and8000',   'no',               'Promising',    0.4],
                         ['Low',    'between3000and8000',   'no',               'NotPromising', 0.6],
                         ['Low',    'more8000',             'yes',              'Promising',    0.7],
                         ['Low',    'more8000',             'yes',              'NotPromising', 0.3],
                         ['Low',    'more8000',             'no',               'Promising',    0.64],
                         ['Low',    'more8000',             'no',               'NotPromising', 0.36]])

    paymentHistory = Node(name='paymentHistory', possible_values=['Good', 'Acceptable', 'Bad'],
                          cpt=
                          [['age',              'ratioOfDebts', 'paymentHistory'],
                           ['between18and30',   'High',         'Good',             0.1],
                           ['between18and30',   'High',         'Acceptable',       0.3],
                           ['between18and30',   'High',         'Bad',              0.6],
                           ['between18and30',   'Low',          'Good',             0.15],
                           ['between18and30',   'Low',          'Acceptable',       0.35],
                           ['between18and30',   'Low',          'Bad',              0.5],
                           ['between30and50',   'High',         'Good',             0.2],
                           ['between30and50',   'High',         'Acceptable',       0.35],
                           ['between30and50',   'High',         'Bad',              0.45],
                           ['between30and50',   'Low',          'Good',             0.3],
                           ['between30and50',   'Low',          'Acceptable',       0.4],
                           ['between30and50',   'Low',          'Bad',              0.3],
                           ['morethan50',       'High',         'Good',             0.35],
                           ['morethan50',       'High',         'Acceptable',       0.4],
                           ['morethan50',       'High',         'Bad',              0.25],
                           ['morethan50',       'Low',          'Good',             0.6],
                           ['morethan50',       'Low',          'Acceptable',       0.3],
                           ['morethan50',       'Low',          'Bad',              0.1]])

    reliable = Node(name='reliable', possible_values=['yes', 'no'],
                    cpt=
                    [['paymentHistory', 'age',              'married',  'guarantor',    'reliable'],
                     ['Good',           'between18and30',   'yes',      'yes',          'yes',      0.4],
                     ['Good',           'between18and30',   'yes',      'yes',          'no',       0.6],
                     ['Good',           'between18and30',   'yes',      'no',           'yes',      0.25],
                     ['Good',           'between18and30',   'yes',      'no',           'no',       0.75],
                     ['Good',           'between18and30',   'no',       'yes',          'yes',      0.2],
                     ['Good',           'between18and30',   'no',       'yes',          'no',       0.8],
                     ['Good',           'between18and30',   'no',       'no',           'yes',      0.15],
                     ['Good',           'between18and30',   'no',       'no',           'no',       0.85],
                     ['Good',           'between30and50',   'yes',      'yes',          'yes',      0.48],
                     ['Good',           'between30and50',   'yes',      'yes',          'no',       0.52],
                     ['Good',           'between30and50',   'yes',      'no',           'yes',      0.38],
                     ['Good',           'between30and50',   'yes',      'no',           'no',       0.62],
                     ['Good',           'between30and50',   'no',       'yes',          'yes',      0.36],
                     ['Good',           'between30and50',   'no',       'yes',          'no',       0.64],
                     ['Good',           'between30and50',   'no',       'no',           'yes',      0.23],
                     ['Good',           'between30and50',   'no',       'no',           'no',       0.77],
                     ['Good',           'morethan50',       'yes',      'yes',          'yes',      0.8],
                     ['Good',           'morethan50',       'yes',      'yes',          'no',       0.2],
                     ['Good',           'morethan50',       'yes',      'no',           'yes',      0.6],
                     ['Good',           'morethan50',       'yes',      'no',           'no',       0.4],
                     ['Good',           'morethan50',       'no',       'yes',          'yes',      0.58],
                     ['Good',           'morethan50',       'no',       'yes',          'no',       0.42],
                     ['Good',           'morethan50',       'no',       'no',           'yes',      0.52],
                     ['Good',           'morethan50',       'no',       'no',           'no',       0.48],
                     ['Acceptable',     'between18and30',   'yes',      'yes',          'yes',      0.35],
                     ['Acceptable',     'between18and30',   'yes',      'yes',          'no',       0.65],
                     ['Acceptable',     'between18and30',   'yes',      'no',           'yes',      0.22],
                     ['Acceptable',     'between18and30',   'yes',      'no',           'no',       0.78],
                     ['Acceptable',     'between18and30',   'no',       'yes',          'yes',      0.15],
                     ['Acceptable',     'between18and30',   'no',       'yes',          'no',       0.85],
                     ['Acceptable',     'between18and30',   'no',       'no',           'yes',      0.1],
                     ['Acceptable',     'between18and30',   'no',       'no',           'no',       0.9],
                     ['Acceptable',     'between30and50',   'yes',      'yes',          'yes',      0.43],
                     ['Acceptable',     'between30and50',   'yes',      'yes',          'no',       0.57],
                     ['Acceptable',     'between30and50',   'yes',      'no',           'yes',      0.35],
                     ['Acceptable',     'between30and50',   'yes',      'no',           'no',       0.65],
                     ['Acceptable',     'between30and50',   'no',       'yes',          'yes',      0.34],
                     ['Acceptable',     'between30and50',   'no',       'yes',          'no',       0.66],
                     ['Acceptable',     'between30and50',   'no',       'no',           'yes',      0.2],
                     ['Acceptable',     'between30and50',   'no',       'no',           'no',       0.8],
                     ['Acceptable',     'morethan50',       'yes',      'yes',          'yes',      0.75],
                     ['Acceptable',     'morethan50',       'yes',      'yes',          'no',       0.25],
                     ['Acceptable',     'morethan50',       'yes',      'no',           'yes',      0.58],
                     ['Acceptable',     'morethan50',       'yes',      'no',           'no',       0.42],
                     ['Acceptable',     'morethan50',       'no',       'yes',          'yes',      0.52],
                     ['Acceptable',     'morethan50',       'no',       'yes',          'no',       0.48],
                     ['Acceptable',     'morethan50',       'no',       'no',           'yes',      0.5],
                     ['Acceptable',     'morethan50',       'no',       'no',           'no',       0.5],
                     ['Bad',            'between18and30',   'yes',      'yes',          'yes',      0.2],
                     ['Bad',            'between18and30',   'yes',      'yes',          'no',       0.8],
                     ['Bad',            'between18and30',   'yes',      'no',           'yes',      0.18],
                     ['Bad',            'between18and30',   'yes',      'no',           'no',       0.82],
                     ['Bad',            'between18and30',   'no',       'yes',          'yes',      0.1],
                     ['Bad',            'between18and30',   'no',       'yes',          'no',       0.9],
                     ['Bad',            'between18and30',   'no',       'no',           'yes',      0.05],
                     ['Bad',            'between18and30',   'no',       'no',           'no',       0.95],
                     ['Bad',            'between30and50',   'yes',      'yes',          'yes',      0.4],
                     ['Bad',            'between30and50',   'yes',      'yes',          'no',       0.6],
                     ['Bad',            'between30and50',   'yes',      'no',           'yes',      0.32],
                     ['Bad',            'between30and50',   'yes',      'no',           'no',       0.68],
                     ['Bad',            'between30and50',   'no',       'yes',          'yes',      0.31],
                     ['Bad',            'between30and50',   'no',       'yes',          'no',       0.69],
                     ['Bad',            'between30and50',   'no',       'no',           'yes',      0.18],
                     ['Bad',            'between30and50',   'no',       'no',           'no',       0.82],
                     ['Bad',            'morethan50',       'yes',      'yes',          'yes',      0.6],
                     ['Bad',            'morethan50',       'yes',      'yes',          'no',       0.4],
                     ['Bad',            'morethan50',       'yes',      'no',           'yes',      0.55],
                     ['Bad',            'morethan50',       'yes',      'no',           'no',       0.45],
                     ['Bad',            'morethan50',       'no',       'yes',          'yes',      0.5],
                     ['Bad',            'morethan50',       'no',       'yes',          'no',       0.5],
                     ['Bad',            'morethan50',       'no',       'no',           'yes',      0.48],
                     ['Bad',            'morethan50',       'no',       'no',           'no',       0.52]])

    creditWorthiness = Node(name='creditWorthiness', possible_values=['Positive', 'Negative'],
                            cpt=[['reliable',   'futureIncome', 'ratioOfDebts', 'creditWorthiness'],
                                 ['yes',        'Promising',    'High',         'Positive',         0.8],
                                 ['yes',        'Promising',    'High',         'Negative',         0.2],
                                 ['yes',        'Promising',    'Low',          'Positive',         0.95],
                                 ['yes',        'Promising',    'Low',          'Negative',         0.05],
                                 ['yes',        'NotPromising', 'High',         'Positive',         0.6],
                                 ['yes',        'NotPromising', 'High',         'Negative',         0.4],
                                 ['yes',        'NotPromising', 'Low',          'Positive',         0.7],
                                 ['yes',        'NotPromising', 'Low',          'Negative',         0.3],
                                 ['no',         'Promising',    'High',         'Positive',         0.6],
                                 ['no',         'Promising',    'High',         'Negative',         0.4],
                                 ['no',         'Promising',    'Low',          'Positive',         0.68],
                                 ['no',         'Promising',    'Low',          'Negative',         0.32],
                                 ['no',         'NotPromising', 'High',         'Positive',         0.4],
                                 ['no',         'NotPromising', 'High',         'Negative',         0.6],
                                 ['no',         'NotPromising', 'Low',          'Positive',         0.5],
                                 ['no',         'NotPromising', 'Low',          'Negative',         0.5]])

    net.add_node(age)
    net.add_node(guarantor)
    net.add_node(married)
    net.add_node(income)
    net.add_node(higherEducation)
    net.add_node(futureIncome)
    net.add_node(reliable)
    net.add_node(ratioOfDebts)
    net.add_node(paymentHistory)
    net.add_node(creditWorthiness)
    net.add_node(assets)
    net.build_net()
    net.show()

    print(net.get_probs('age', {}))
    print(net.get_probs('guarantor', {}))
    print(net.get_probs('married', {}))
    print(net.get_probs('income', {}))
    print(net.get_probs('higherEducation', {}))
    print(net.get_probs('futureIncome', {}))
    print(net.get_probs('reliable', {}))
    print(net.get_probs('ratioOfDebts', {}))
    print(net.get_probs('paymentHistory', {}))
    print(net.get_probs('creditWorthiness', {}))
    print(net.get_probs('assets', {}))


    print('===================================')

    print(net.get_probs('age', {'creditWorthiness': 'Positive', 'income': 'less1000'}))


